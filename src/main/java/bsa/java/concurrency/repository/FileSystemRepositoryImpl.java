package bsa.java.concurrency.repository;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component
public class FileSystemRepositoryImpl implements FileSystemRepository {
    @Value("${images.folder}")
    private String PATH;

    @Override
    public CompletableFuture<Path> saveFile(UUID uuid, byte[] file) {
        return CompletableFuture.supplyAsync(() -> {
                    try {
                        findOrCreateFolder(PATH);
                        Path filePath = getPathByUUID(uuid);
                        Files.createFile(filePath);
                        Files.write(filePath, file);
                        return filePath;
                    } catch (Exception e) {
                        throw new RuntimeException(e.getMessage());
                    }
                }
        );
    }

    private File findOrCreateFolder(String folderName) {
        File folder = new File(folderName);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder;
    }

    public void deleteById(UUID uuid) {
        try {
            Files.deleteIfExists(getPathByUUID(uuid));
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private Path getPathByUUID(UUID uuid) {
        return Paths.get(PATH + uuid.toString() + ".jpg");
    }

    public void deleteAll() {
        try {
            FileUtils.cleanDirectory(new File(PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
