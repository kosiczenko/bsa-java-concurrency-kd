package bsa.java.concurrency.repository;

import java.nio.file.Path;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface FileSystemRepository {

    CompletableFuture<Path> saveFile(UUID uuid, byte[] file);
    void deleteById(UUID uuid);
    void deleteAll();

}
