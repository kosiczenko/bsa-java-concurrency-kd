package bsa.java.concurrency.image;

import lombok.Data;
import java.util.UUID;

@Data
public class Image {
    private UUID id;

    private String path;
    private long hash;

    public Image() {
    }

    public Image(UUID id, String path, long hash) {
        this.id = id;
        this.path = path;
        this.hash = hash;
    }

    @Override
    public String toString() {
        return path + "(" + hash + ")";
    }
}
