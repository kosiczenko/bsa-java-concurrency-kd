package bsa.java.concurrency.image;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


@RestController
@RequestMapping("/images")
public class ImagesController {

    @GetMapping(value = "/{fileName}", produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody byte[] getJpegImage(@PathVariable String fileName) throws IOException {
        File jpgFile = new File("images/" + fileName);
        InputStream in = new FileInputStream(jpgFile);
        return org.apache.commons.io.IOUtils.toByteArray(in);
    }
}
