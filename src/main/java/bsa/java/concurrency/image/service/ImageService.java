package bsa.java.concurrency.image.service;

import bsa.java.concurrency.repository.FileSystemRepository;
import bsa.java.concurrency.image.Image;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.dto.SearchResultDtoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class ImageService {
    private final DHasher dHasher;
    private final FileSystemRepository fileSystem;
    private final List<Image> imagesList = new LinkedList<>();

    @Value("${images.url}")
    private String IMAGES_URL;

    @Autowired
    public ImageService(DHasher dHasher, FileSystemRepository fileSystem) {
        this.dHasher = dHasher;
        this.fileSystem = fileSystem;
    }

    public void saveAll(MultipartFile[] files) {
        Arrays.asList(files)
                .parallelStream()
                .forEach(this::saveImage);
    }

    public void saveImage(MultipartFile file) {

        try {
            var bytesArray = file.getBytes();

            CompletableFuture<Long> hashFuture = CompletableFuture.supplyAsync(
                    () -> dHasher.calculateHash(bytesArray)
            );

            var uuid = UUID.randomUUID();
            var saveFileFuture = fileSystem.saveFile(uuid, bytesArray);

            long hash = hashFuture.get(1000, TimeUnit.MILLISECONDS);
            Path pathTo = saveFileFuture.get(1000, TimeUnit.MILLISECONDS);

            imagesList.add(new Image(uuid, pathTo.toString(), hash));

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public SearchResultDTO searchMatches(MultipartFile file, double threshold) {

        try {
            var bytesArray = file.getBytes();
            long hash = dHasher.calculateHash(bytesArray);
            double match;

            if (imagesList.isEmpty()) {
                match = 0;
            } else {
                match = imagesList
                        .stream()
                        .map(image -> dHasher.calculateHamming(hash, image.getHash()))
                        .max(Comparator.naturalOrder())
                        .get();

                if (match > threshold) {
                    return SearchResultDtoImpl
                            .builder()
                            .id(null)
                            .match(match)
                            .image(IMAGES_URL)
                            .build();
                }
            }

            var uuid = UUID.randomUUID();
            Path pathTo = fileSystem.saveFile(uuid, bytesArray).get();
            var image = new Image(uuid, pathTo.toString(), hash);

            imagesList.add(image);

            return SearchResultDtoImpl.builder()
                    .id(uuid)
                    .match(match)
                    .image(IMAGES_URL+uuid.toString()+".jpg")
                    .build();

        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteById(UUID uuid) {
        imagesList.removeIf(image -> image.getId() == uuid);
        fileSystem.deleteById(uuid);
    }

    public void deleteAll() {
        imagesList.clear();
        fileSystem.deleteAll();
    }
}
