package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.image.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/image")
public class ImageController {

    private ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public String batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        imageService.saveAll(files);
        return "Files loaded.";
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(
            @RequestParam("image") MultipartFile multipartFile,
            @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {

        return List.of(imageService.searchMatches(multipartFile, threshold));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public String deleteImage(@PathVariable("id") UUID imageId) {
        imageService.deleteById(imageId);
        return "Deleted";
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public String purgeImages(){
        imageService.deleteAll();
        return "All clear";
    }

    @GetMapping("/image")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<UUID> image(){
        return new ResponseEntity<>(UUID.randomUUID(), HttpStatus.OK);
    }
}
