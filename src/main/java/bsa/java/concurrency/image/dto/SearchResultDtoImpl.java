package bsa.java.concurrency.image.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class SearchResultDtoImpl implements SearchResultDTO {
    UUID id;
    String image;
    Double match;
}
